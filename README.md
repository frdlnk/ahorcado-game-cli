# Ahorcado CLI

Este es el proyecto programado para el curso IF1300 del bachillerato de informática empresarial de la Universidad de Costa Rica.


Es un juego que se ejecuta en la terminal del sistema operativo y el fin de este juego es imitar al juego de ahorcado. El usuario deberá ingresar un username e intentar adivinar cuál es la palabra


## Especificaciones

El juego está escrito todo completamente en Java, usando el JDK 22

## Autores

Freddy Línkemer
Isaac Mayorga
César Gonzáles

## Licencia
Licensia MIT, este código es abierto a todos y se puede modificar al gusto.