package game;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileWriter;
import java.util.Scanner;
import java.io.FileReader;
import java.util.ArrayList;

public class RequestUsername {

    // Lee los nombres de usuario desde el archivo JSON
    public ArrayList<String> readNamesFromRegistry(){
        JSONParser parser = new JSONParser();
        try {
            FileReader reader = new FileReader("data.json");
            Object fileObj = parser.parse(reader);
            JSONObject fileJson = (JSONObject) fileObj;

            return (ArrayList<String>) fileJson.get("nombres");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Escribe un nuevo nombre de usuario en el archivo JSON
    public void writeNewName(String name) {
        JSONParser parser = new JSONParser();
        try {
            Object file = parser.parse(new FileReader("data.json"));
            JSONObject fileJson = (JSONObject) file;
            ArrayList<String> existentNames = readNamesFromRegistry();
            existentNames.add(name.toLowerCase());
            fileJson.put("nombres", existentNames);

            FileWriter writer = new FileWriter("data.json");
            writer.write(fileJson.toJSONString());
            writer.close();
        }
        catch (Exception e) {
            return;
        }
    }

    // Pide al usuario que ingrese su nombre de usuario
    public Object askUsername(){
        ArrayList<String> names = readNamesFromRegistry();
        System.out.print("Ingrese su usuario: ");
        Scanner readname = new Scanner(System.in);
        String nombre_usuario = readname.nextLine();
        if(names.contains(nombre_usuario.toLowerCase())) return null;
        else {
            writeNewName(nombre_usuario);
            return nombre_usuario;
        }
    }
}
