package game;

import data.PlayerData;
import org.json.simple.JSONObject;

public class GameCLI {
    Game game = new Game();
    RequestUsername rqun = new RequestUsername();
    PlayerData playerData = new PlayerData();
    Fame fame = new Fame();

    // Inicia el juego
    public void startGame() {
        boolean playerDataIsSet = false;

        while (!playerDataIsSet) {
            Object playerName = rqun.askUsername();
            if(playerName == null) {
                System.out.println("Ese nombre de usuario ya existe");
                continue;
            }
            else {
                playerData.jugador = (String) playerName;

                playerDataIsSet = true;
            }
        }
        JSONObject data = game.gameCLI();
        playerData.palabra = (String) data.get("palabra");
        playerData.intentos = (int) data.get("intentos");

        fame.writeOnSaloonFame(playerData);
    }
}
