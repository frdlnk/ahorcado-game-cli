package game;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class FameSaloonCLI {
    Fame fame = new Fame();

    // Inicia el salón de la fama
    public void startFameSaloon() {
        JSONArray data = fame.readFameSaloon();
        System.out.println("Este es el salon de la fama:");
        for(Object d : data) {
            JSONObject dJson = (JSONObject) d;
            System.out.println(String.format("Jugador: %s, Palabra: %s, Intentos necesitados: %s", dJson.get("jugador"), dJson.get("palabra"), dJson.get("intentos")));
        }
    }
}
