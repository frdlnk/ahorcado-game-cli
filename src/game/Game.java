package game;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    private WordHandler werhnd = new WordHandler();
    private String wrdToGuess = werhnd.randomWordGenerator().toString().toUpperCase();
    private int intentos = 0;
    private JSONObject data = new JSONObject();

    //Se crea un banco de palabras actualizable para mostrar al jugador
    private String lttsToUse(ArrayList<Character> abcs){
        StringBuilder letters = new StringBuilder("A B C D E F G H I\nJ K L M N O P Q R\n S T U V W X Y Z");
        for (char ltt : abcs) {
            int index = letters.indexOf(String.valueOf(ltt));
            letters.replace(index, index + 1, " ");
        }
        return letters.toString();
    }

    //En donde se muestra al jugador el CLI del juego y procesa sus respuestas
    public JSONObject gameCLI() {
        Scanner readAwn = new Scanner(System.in);
        int cliIntento;
        String cliWrd = "";

        // Crea los renglones de la palabra que se ve
        for (int indice = 1; indice <= wrdToGuess.length(); indice++) {
            cliWrd += "_";
        }

        //lista de letras ya ingresadas por el usuario
        ArrayList<Character> lttsUsed = new ArrayList<>();

        //constructor secundario para el banco de palabras
        StringBuilder wrdCheck = new StringBuilder();

        while (intentos < 5) {
            //lo que el usuario ve al jugar
            cliIntento = 5 - intentos;
            System.out.println(lttsToUse(lttsUsed) + "\n");
            System.out.println(cliWrd);
            System.out.println("Intentos restantes: " + cliIntento);
            System.out.print("Ingrese su letra a adivinar: ");

            char guess = readAwn.next().toUpperCase().charAt(0);

            //error por si la letra ingresada ya fue ingresada antes
            if (lttsUsed.contains(guess)){
                System.out.println("Ya ingresaste esa letra");
                continue;
            }
            //agrega la letra ingresada a la lista de ya usadas
            lttsUsed.add(guess);

            //examina la palabra correcta a ver si la letra ingresada está
            if (wrdToGuess.contains(String.valueOf(guess))){
                //actualiza a los renglones vacíos y llena los necesarios, usando el banco de palabras
                for (int indice = 0; indice < wrdToGuess.length(); indice++){
                    StringBuilder stringBuilder = wrdToGuess.charAt(indice) == guess ? wrdCheck.append(guess) : wrdCheck.append(cliWrd.charAt(indice));
                }
                //actualiza los renglones y reinicia el constructor
                cliWrd = wrdCheck.toString();
                wrdCheck.setLength(0);

                //la pantalla de victoria
                if (cliWrd.equals(wrdToGuess)){
                    data.put("palabra", wrdToGuess);
                    data.put("intentos", intentos);
                    System.out.println("Pegaste la palabra");
                    return data;
                }
            }
            else {
                intentos++;
                System.out.println("La letra no esta en la palabra");
            }
        }
        System.out.println("Se te acabaron los intentos, la palabra era " + wrdToGuess);
        return null;
    }

}
