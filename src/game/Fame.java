package game;

import data.PlayerData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Fame {

    // Lee la fama del salón desde el archivo JSON
    public JSONArray readFameSaloon() {
        JSONParser parser = new JSONParser();

        try {
            FileReader reader = new FileReader("data.json");
            Object fileObj = parser.parse(reader);
            JSONObject fileJson = (JSONObject) fileObj;

            JSONArray fame = (JSONArray) fileJson.get("fama");

            ArrayList sortedFame = sortFameSaloon(fame);

            return (JSONArray) sortedFame;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Escribe en el salón de la fama
    public void writeOnSaloonFame(PlayerData playerData) {
        JSONParser parser = new JSONParser();
        try {
            Object file = parser.parse(new FileReader("data.json"));
            JSONObject player = new JSONObject();
            player.put("jugador", playerData.jugador);
            player.put("palabra", playerData.palabra);
            player.put("intentos", playerData.intentos);

            JSONObject fileJson = (JSONObject) file;
            ArrayList<JSONObject> players = (ArrayList<JSONObject>) fileJson.get("fama");
            players.add(player);

            fileJson.put("fama", players);

            FileWriter writer = new FileWriter("data.json");
            writer.write(fileJson.toJSONString());
            writer.close();
        }
        catch (Exception e) {
            return;
        }
    }

    // Ordena el salón de la fama por número de intentos
    private ArrayList<JSONObject> sortFameSaloon(ArrayList<JSONObject> array) {
        Collections.sort(array, (o1, o2) -> {
            Long attemptsUser1 = (Long) o1.get("intentos");
            Long attemptsUser2 = (Long) o2.get("intentos");

            return attemptsUser1.compareTo(attemptsUser2);
        });
        return array;
    }
}
