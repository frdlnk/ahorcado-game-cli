package game;

import words.Words;

import java.util.ArrayList;
import java.util.Random;

public class WordHandler {

    // Genera una palabra aleatoria
    public Object randomWordGenerator(){
        Words words = new Words();
        Random rand = new Random();
        ArrayList<String> wordList = words.readWordsFromRegistry();
        int index = rand.nextInt(wordList.size());
        return wordList.get(index);
    }
}
