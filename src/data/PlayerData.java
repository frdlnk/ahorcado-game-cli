package data;

import lombok.Getter;
import lombok.Setter;

public class PlayerData {
    @Getter @Setter
    public String jugador;

    @Setter @Getter
    public String palabra;

    @Setter @Getter
    public int intentos;
}
