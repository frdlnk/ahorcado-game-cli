package words;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;

public class Words {

    // Lee palabras desde el archivo de registro JSON
    public ArrayList<String> readWordsFromRegistry(){
        JSONParser parser = new JSONParser();
        try {
            FileReader reader = new FileReader("data.json");
            Object fileObj = parser.parse(reader);
            JSONObject fileJson = (JSONObject) fileObj;

            return (ArrayList<String>) fileJson.get("palabras");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // Escribe una nueva palabra en el archivo JSON
    public boolean writeNewWord(String word) {
        JSONParser parser = new JSONParser();
        try {
            Object file = parser.parse(new FileReader("data.json"));
            JSONObject fileJson = (JSONObject) file;
            ArrayList<String> existentWords = readWordsFromRegistry();
            existentWords.add(word.toLowerCase());
            fileJson.put("palabras", existentWords);

            FileWriter writer = new FileWriter("data.json");
            writer.write(fileJson.toJSONString());
            writer.close();

            System.out.println(String.format("La palabra %w ha sido agregada", word));
        }
        catch (Exception e) {
            return false;
        }
        return false;
    }

    // Modifica una palabra existente en el archivo JSON
    public Object modifyExistentWord(int indexOfWord, String newWord){
        JSONParser parser = new JSONParser();
        try {
            Object file = parser.parse(new FileReader("data.json"));
            JSONObject fileJson = (JSONObject) file;
            ArrayList<String> wordsRegistry = readWordsFromRegistry();
            if(indexOfWord > wordsRegistry.size()) return "No existe palabta en ese índice";
            else {
                wordsRegistry.set(indexOfWord, newWord.toLowerCase());
                fileJson.put("palabras", wordsRegistry);
                FileWriter writer = new FileWriter("data.json");
                writer.write(fileJson.toJSONString());
                writer.close();

                return "Se actualizó la palabra";
            }
        }
        catch (Exception e) {
            return e;
        }
    }
}
