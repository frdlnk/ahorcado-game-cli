package words;

import java.util.ArrayList;
import java.util.Scanner;

public class WordsCLI {

    Words words = new Words();

    // Muestra las palabras disponibles en el registro
    public void showAvailableWords(){
        System.out.println(String.format("Estas son las palabras disponibles"));
        ArrayList availableWords = words.readWordsFromRegistry();
        availableWords.forEach(word -> {
            System.out.println(String.format("%s - %s", availableWords.indexOf(word), word));
        });

        System.out.println(String.format("Recuerda que puedes ingresar nuevas palabras o modificar las que ya están"));

        // Ejecución del menú
    }

    // Permite escribir o modificar una palabra
    public void writeOrModifyWord(String status) {
        Scanner scanner = new Scanner(System.in);
        if(status.equals("create")) {
            System.out.println("Ingrese la palabra a añadir: ");
            System.out.println(words.writeNewWord(scanner.nextLine()));
        }
        else {
            this.showAvailableWords();
            System.out.println("Ingrese el indice de la palabra a modificar");
            int wordToModify = scanner.nextInt();
            System.out.println("Ingrese la palabra para sustituir");
            scanner.nextLine();
            String wordToReplace = scanner.nextLine();
            Object res = words.modifyExistentWord(wordToModify, wordToReplace);
            System.out.println(res);
        }
    }
}
