package menu;

import game.FameSaloonCLI;
import game.GameCLI;
import words.WordsCLI;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Menu {

    WordsCLI wcli = new WordsCLI();
    GameCLI gcli = new GameCLI();
    FameSaloonCLI fcli = new FameSaloonCLI();

    // Lee un archivo línea por línea y lo imprime en la consola
    private void readFile(String file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lee el menú desde un archivo y lo muestra en la consola
    private void readMenu() {
        try (BufferedReader br = new BufferedReader(new FileReader("menu.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Menú de palabras
    private void WordsMenu() {
        readFile("wordsMenu.txt");
        String opt = new Scanner(System.in).nextLine().toUpperCase();
        switch (opt) {
            case "P":
                wcli.showAvailableWords();
                WordsMenu();
            case "J":
                wcli.writeOrModifyWord("modify");
                WordsMenu();
            case "F":
                wcli.writeOrModifyWord("create");
                WordsMenu();

            case "M":
                this.HandleOptions();
        }
    }

    // Lee las instrucciones desde un archivo y las muestra en la consola
    private void readInstrucctions() {
        readFile("instrucciones.txt");
        switch (new Scanner(System.in).nextLine().toUpperCase()) {
            case "M":
                this.HandleOptions();
        }
    }

    // Maneja las opciones del menú principal
    public void HandleOptions() {
        readFile("menu.txt");
        switch (new Scanner(System.in).nextLine().toUpperCase()) {
            case "I":
                WordsMenu();
                break;
            case "P":
                gcli.startGame();
                break;
            case "J":
                this.readInstrucctions();
                break;
            case "F":
                fcli.startFameSaloon();
                this.HandleOptions();
                break;
            default:
                System.exit(0);
        }
    }
}
